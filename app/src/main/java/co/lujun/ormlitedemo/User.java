package co.lujun.ormlitedemo;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Administrator on 2015/9/15.
 */
@DatabaseTable(tableName = "user")// 数据库中的user表
public class User {

    @DatabaseField(generatedId = true)// 为主键且自动生成
    private int id;
    @DatabaseField(columnName = "name")// 列名为name
    private String name;
    @DatabaseField(columnName = "desc")// 列名为desc
    private String desc;

    @ForeignCollectionField// 关联一个集合，在查询User的时候，一并获取articles的值
    private Collection<Article> articles;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Collection<Article> getArticles() {
        return articles;
    }

    public void setArticles(Collection<Article> articles) {
        this.articles = articles;
    }

    @Override
    public String toString()
    {
        return "User [id=" + id + ", name=" + name + ", desc= " + desc + "]";
    }
}
