package co.lujun.ormlitedemo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by Administrator on 2015/9/15.
 */
public class DBHelper extends OrmLiteSqliteOpenHelper {

    private final static String DB_NAME = "db_test.db";

    private Dao<User, Integer> userDao;

    private DBHelper(Context context){
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTableIfNotExists(connectionSource, User.class);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i1) {
        try {
            TableUtils.dropTable(connectionSource, User.class, true);
            onCreate(sqLiteDatabase, connectionSource);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private static DBHelper mDataBaseHelper;

    public static DBHelper getDataBaseHelper(Context context){
        if (mDataBaseHelper == null){
            synchronized (DBHelper.class){
                if (mDataBaseHelper == null){
                    mDataBaseHelper = new DBHelper(context);
                }
            }
        }
        return mDataBaseHelper;
    }

    public Dao<User, Integer> getUserDao() throws SQLException{
        if (userDao == null){
            userDao = getDao(User.class);
        }
        return userDao;
    }

    @Override
    public void close() {
        super.close();
        userDao = null;
    }
}
