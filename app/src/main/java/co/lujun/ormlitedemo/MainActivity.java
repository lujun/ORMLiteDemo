package co.lujun.ormlitedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //http://blog.csdn.net/lmj623565791/article/details/39122981

        try{
            //DBHelper helper = DBHelper.getDataBaseHelper(this);
            //insert
            /*User user = new User("lujun", "22222222");
            helper.getUserDao().create(user);
            user = new User("lujun1", "22222222");
            helper.getUserDao().create(user);
            user = new User("lujun2", "22222222");
            helper.getUserDao().create(user);
            user = new User("lujun3", "22222222");
            helper.getUserDao().create(user);
            user = new User("lujun4", "22222222");
            helper.getUserDao().create(user);
            user = new User("lujun5", "22222222");
            helper.getUserDao().create(user);
            user = new User("lujun6", "22222222");
            helper.getUserDao().create(user);

            //query
            List<User> users = helper.getUserDao().queryForAll();
            Log.d("OrmTest", users.toString());*/

            //update
            /*User user = new User("lujun-android", "haishi2222222");
            user.setId(2);
            helper.getUserDao().update(user);*/

            //delete
//            helper.getUserDao().deleteById(2);
//            helper.getUserDao().refresh(null);


            /////////////////////////////
            /*User user = new User();
            user.setName("lujun");
            DatabaseHelper.getDatabaseHelper(this).getDao(User.class).create(user);
            Article article = new Article();
            article.setTitle("Android Book");
            article.setUser(user);
            DatabaseHelper.getDatabaseHelper(this).getDao(Article.class).create(article);*/

            /*Article article = (Article)DatabaseHelper.getDatabaseHelper(this).getDao(Article.class).queryForId(2);
            Toast.makeText(this, article.getTitle() + article.getUser().getName(), Toast.LENGTH_SHORT).show();*/

            //collect test
            /*User user = (User)DatabaseHelper.getDatabaseHelper(this).getDao(User.class).queryForId(2);
            String s = "username:" + user.getName() + "   articles:";
            if (user.getArticles() != null){
                for (Article article : user.getArticles()){
                    s += article.getTitle() + ",";
                }
            }
            Toast.makeText(this, s, Toast.LENGTH_SHORT).show();*/

            //QueryBuilder test
            /*List<Article> articles = DatabaseHelper.getDatabaseHelper(this).getDao(Article.class)
                    .queryBuilder().where().eq("user_id", 2).query();
            Toast.makeText(this, articles.size() + "", Toast.LENGTH_SHORT).show();*/

           /* QueryBuilder<Article, Integer> queryBuilder =
                    DatabaseHelper.getDatabaseHelper(this).getDao(Article.class).queryBuilder();
            Where<Article, Integer> where = queryBuilder.where();
            where.eq("user_id", 2);
            where.and();
            where.eq("id", 4);*/
            //或者
            /*List<Article> articles = DatabaseHelper.getDatabaseHelper(this).getDao(Article.class)
                    .queryBuilder().where().eq("user_id", 2).and().eq("id", 4).query();

            Toast.makeText(this, articles.size() + "", Toast.LENGTH_SHORT).show();*/

            //updateBuilder、deleteBuilder
//            DatabaseHelper.getDatabaseHelper(this).getDao(Article.class).updateRaw("sql","arguments");


            //事务操作
            TransactionManager.callInTransaction(
                    DatabaseHelper.getDatabaseHelper(this).getConnectionSource(), new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {

                            return null;
                        }
                    });


        }catch (SQLException e){
            e.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
