package co.lujun.ormlitedemo;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Administrator on 2015/9/15.
 */
@DatabaseTable(tableName = "article")
public class Article {

    @DatabaseField(generatedId = true)//id=true表示为主键
    private int id;
    @DatabaseField(columnName = "title")
    private String title;
    @DatabaseField(canBeNull = true, foreign = true, columnName = "user_id", foreignAutoRefresh = true)
    //canBeNull-能否为null；foreign=true表示是一个外键;columnName 列名；
    // foreignAutoRefresh，此处User为外键，当拿到Article对象时就直接拿到了User对象
    //否则需要在拿到Article对象之后还需要刷新才能拿到User对象：helper.getDao(User.class).refresh(article.getUser());
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Srticle [id=" + id + ", title=" + title +", user=" + user + "]";
    }
}
