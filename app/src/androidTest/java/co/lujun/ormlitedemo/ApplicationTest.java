package co.lujun.ormlitedemo;

import android.app.Application;
import android.test.ApplicationTestCase;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }

    //测试方法必须以test开头
    public void test() throws Exception{
        final int expected = 1;
        final int reality = 3;
        assertEquals(expected, reality);
    }
}